from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt

def plotter(p1data1,p1data2,p1m1,p1cov,p1m2,titl,sz):
    fig = plt.figure()
    ax = plt.axes(projection="3d")
    plt.title(titl)
    ax.scatter3D(p1data1[:,0],p1data1[:,1],p1data1[:,2],marker='x',s=sz)
    ax.scatter3D(p1data2[:,0],p1data2[:,1],p1data2[:,2],marker='o',s=sz)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    fig = plt.figure()
    plt.title(titl+" projection on XY")
    plt.scatter(p1data1[:,0],p1data1[:,1],marker = 'x',s=sz)
    plt.scatter(p1data2[:,0],p1data2[:,1],marker = 'o',s=sz)
    plt.xlabel("X")
    plt.ylabel("Y")

    fig = plt.figure()
    plt.title(titl+" projection on YZ")
    plt.scatter(p1data1[:,1],p1data1[:,2],marker = 'x',s=sz)
    plt.scatter(p1data2[:,1],p1data2[:,2],marker = 'o',s=sz)
    plt.xlabel("Y")
    plt.ylabel("Z")

    fig = plt.figure()
    plt.title(titl+" projection on XZ")
    plt.scatter(p1data1[:,0],p1data1[:,2],marker = 'x',s=sz)
    plt.scatter(p1data2[:,0],p1data2[:,2],marker = 'o',s=sz)
    plt.xlabel("X")
    plt.ylabel("Z")
    plt.show()


p1cov = [[3,0,0],[0,7,0],[0,0,10]]
p1m1 = [3,7,10]
p1m2 = [6,9,7]
p2cov = [[4,1,-1],[1,2,1],[-1,1,2]]
p3cov = [[2,1,0],[1,5,1],[0,1,8]]

p1data1 = np.random.multivariate_normal(p1m1,p1cov,1000)
p1data2 = np.random.multivariate_normal(p1m2,p1cov,1000)
plotter(p1data1,p1data2,p1m1,p1cov,p1m2,"Part1: Different means, same diagonal covariance matrix:",10)

p1data1 = np.random.multivariate_normal(p1m1,p2cov,1000)
p1data2 = np.random.multivariate_normal(p1m2,p2cov,1000)
plotter(p1data1,p1data2,p1m1,p1cov,p1m2,"Part2: Different means, same PSD covariance matrix:",10)

p1data1 = np.random.multivariate_normal(p1m1,p2cov,1000)
p1data2 = np.random.multivariate_normal(p1m1,p3cov,1000)
plotter(p1data1,p1data2,p1m1,p1cov,p1m2,"Part3: Same mean,different covariance matrices:",10)