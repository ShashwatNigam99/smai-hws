#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np


# In[2]:


X, y = make_blobs(n_samples=10,
                  n_features=2,
                  centers=2,
                  cluster_std=1,
                  center_box=(-10.0, 10.0),
                  shuffle=True,
                  random_state=1)  # For reproducibility
plt.scatter(X[:,0],X[:,1],c=y)


# In[4]:


range_n_clusters = np.arange(2,10,1)
print(range_n_clusters)
avgs = []
for n_clusters in range_n_clusters:
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(X)
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,"The average silhouette_score is :", silhouette_avg)
    avgs.append(silhouette_avg)
    
# plt.figure(figsize=(10,10))    
plt.plot(range_n_clusters,avgs)
plt.xlabel("K value")
plt.ylabel("Silhouette score")
plt.title("Score vs K value")
plt.grid()

