#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn import metrics
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


def read_data(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    
    num_points = len(lines)
    dim_points = 28 * 28
    data = np.empty((num_points, dim_points))
    labels = np.empty(num_points)
    
    for ind, line in enumerate(lines):
        num = line.split(',')
        labels[ind] = int(num[0])
        data[ind] = [ int(x) for x in num[1:] ]
        
    return (data, labels)

train_data, train_labels = read_data("sample_train.csv")
test_data, test_labels = read_data("sample_test.csv")
print(train_data.shape, test_data.shape)
print(train_labels.shape, test_labels.shape)


# In[3]:


idx = np.random.randint(600, size=100)
reduced_data = train_data[train_labels==0][idx]
reduced_labels = train_labels[train_labels==0][idx]
for i in range(1,10):
    reduced_data = np.vstack((reduced_data,train_data[train_labels==i][idx]))
    reduced_labels = np.concatenate((reduced_labels,train_labels[train_labels==i][idx]),axis=None)
print(reduced_data.shape)    
print(reduced_labels.shape)


# In[4]:


pca = PCA(n_components=2, svd_solver='full')
pca_data = pca.fit_transform(reduced_data)
pca_data.shape


# In[5]:


means = [np.mean(pca_data[reduced_labels==0],axis=0)]
for i in range(1,10):
    means.append(np.mean(pca_data[reduced_labels==i],axis=0))
means = np.array(means)
type(means)


# In[6]:


plt.figure(figsize=(10,10))
plt.scatter(pca_data[:,0],pca_data[:,1],c=reduced_labels,cmap ='tab10' )
plt.title("Ground truth")
plt.show()


# In[7]:


def kmns(data,labels,p):
    kmeans = KMeans(n_clusters=10, random_state=p).fit(data)
    predictions = kmeans.labels_
    predictions.shape
    plt.figure(figsize=(10,10))
    plt.scatter(pca_data[:,0],pca_data[:,1],c=predictions,cmap ='tab10' )
    plt.title("Predictions with initialization random state:"+str(p))
    plt.show()
    score1 = metrics.adjusted_rand_score(labels,predictions)
    print(score1)
    score2 = metrics.adjusted_mutual_info_score(labels,predictions)
    print(score2)
    score3 = metrics.v_measure_score(labels,predictions)
    print(score3)
    return [score1,score2,score3]
 


# In[8]:


stable = []
for r in [1,3,5,21,42]:
    p = kmns(pca_data,reduced_labels,r)
    stable.append(p)
df = pd.DataFrame(stable)
df.columns=["Rand_Score","Mutual Information","V_measure"]


# In[9]:


print(df)


# In[10]:


kmeans = KMeans(n_clusters=10,init = means).fit(pca_data)
predictions = kmeans.labels_
plt.figure(figsize=(10,10))
plt.scatter(pca_data[:,0],pca_data[:,1],c=predictions,cmap ='tab10' )
plt.title("Predictions with initialization as GT")
plt.show()

