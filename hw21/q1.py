import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from sklearn.datasets.samples_generator import make_blobs
from sklearn import metrics

def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)

X, y = make_blobs(n_samples=10, centers=3, n_features=2, random_state=0)
# plt.scatter(X[:, 0], X[:, 1], c=y)
# plt.show()
# plt.grid()
centroids = np.vstack((np.random.uniform(low=-5, high=5, size=(3,)), np.random.uniform(low=-5, high=5, size=(3,)))).T
centroids_old = np.zeros(centroids.shape)

clusters = np.zeros(10)
error = dist(centroids, centroids_old, None)
n=10
k=3

while error >= 0.0001:
    for i in range(n):
        distances = dist(X[i], centroids)
        cluster = np.argmin(distances)
        clusters[i] = cluster
        centroids_old = deepcopy(centroids)
    for i in range(k):
        points = [X[j] for j in range(len(X)) if clusters[j] == i]
        centroids[i] = np.mean(points, axis = 0)
    print(clusters)    
    error = dist(centroids, centroids_old, None)


score = metrics.adjusted_rand_score(y, clusters)
print(score)
plt.scatter(X[:, 0], X[:, 1], c=clusters)
plt.show()
plt.grid()
