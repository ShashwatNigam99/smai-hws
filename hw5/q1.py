import numpy as np
from math import log,pi
import matplotlib.pyplot as plt
m1 = np.array([3,3])
m2 = np.array([7,7])

sig1 = np.array([[3,0],[0,3]])
sig2 = np.array([[3,1],[2,3]])
sig3 = np.array([[7,2],[1,7]])

data = np.random.uniform(low=[0,0],high =[10,10],size=(500,2) )

det1 = np.linalg.det(sig1)
det2 = np.linalg.det(sig2)
det3 = np.linalg.det(sig3)

print(det2)
print(det3)

inv1 = np.linalg.inv(sig1)
inv2 = np.linalg.inv(sig2)
inv3 = np.linalg.inv(sig3)

print(inv2)
print(inv3)



# Part 1

test_labels1 = np.zeros(500)
for i in range(500):
    p1 = -0.5*( ((data[i]-m1).T)@(inv1)@(data[i]-m1) ) - 0.5*(2*np.log(2*pi)+np.log(det1))
    p2 = -0.5*( ((data[i]-m2).T)@(inv1)@(data[i]-m2) ) - 0.5*(2*np.log(2*pi)+np.log(det1))
    if p1>=p2:
        test_labels1[i] = 1
    else:
        test_labels1[i] = 2    

classa = data[test_labels1==1]
classb = data[test_labels1==2]
plt.scatter(classa[:,0],classa[:,1])
plt.scatter(classb[:,0],classb[:,1])
plt.show()



# Part 2
test_labels2 = np.zeros(500)
for i in range(500):
    p1 = -0.5*( ((data[i]-m1).T)@(inv2)@(data[i]-m1) ) - 0.5*(2*log(2*pi)+np.log(det2))
    p2 = -0.5*( ((data[i]-m2).T)@(inv3)@(data[i]-m2) ) - 0.5*(2*log(2*pi)+np.log(det3))
    if p1>=p2:
        test_labels2[i] = 1
    else:
        test_labels2[i] = 2  

classa = data[test_labels2==1]
classb = data[test_labels2==2]
plt.scatter(classa[:,0],classa[:,1])
plt.scatter(classb[:,0],classb[:,1])
plt.show()
