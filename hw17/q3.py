#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


def plotter(d,sigma,length):
    alpha = np.random.uniform(0,1,d)
    alpha = alpha/np.sum(alpha)
    prices = np.random.uniform(17,23,d)

    for _ in range(length):
        noise = np.random.normal(0,sigma)
        new_val = prices[::-1][:d].T @ alpha + noise
        prices = np.append(prices,new_val)

    y = []
    X = []
    for i in range(prices.shape[0]-d):
        y.append(prices[i+d])
        X.append(prices[:i+d][::-1][:d])
    X = np.array(X)
    y = np.array(y)

    reg = LinearRegression().fit(X, y)

    plt.plot(y)
#     preds = X @ np.array(reg.coef_)+reg.intercept_
    preds = reg.predict(X)
    error = np.sum((y-preds)**2)
    
    plt.plot(preds)
    plt.show()
    score = reg.score(X, y)
    print("d = :",d)
    print("Score is :",score)
    print("Error is :",error)
    return score,error


# In[3]:


errors = []
input_d = [1,2,3,4,5,6,7,8,9,10,13,15]
for i in input_d:
    s,e = plotter(i,0.5,1000)
    errors.append(s)


# In[5]:


plt.plot(input_d,errors)
plt.title("accuracy vs d")

