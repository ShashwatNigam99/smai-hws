import matplotlib.pyplot as plt
import numpy as np

def gradient_descent(x, alpha):
	max_iterations = 30
	criteria = []
	error = []

	for i in range(30):
		criteria.append(-2*x*alpha)
		x = x - alpha*2*x
		error.append(x**2)

	return (criteria, error)


alpha = 0.1
x = -2


criteria, error = gradient_descent(x, alpha)
criteria1, error1 = gradient_descent(x, 1)
criteria2, error2 = gradient_descent(x, 1.01)

fig1 = plt.figure()
ax1 = fig1.add_subplot(1,1,1)

fig2 = plt.figure()
ax2 = fig2.add_subplot(1,1,1)

fig3 = plt.figure()
ax3 = fig3.add_subplot(1,1,1)

ax1.plot(criteria, label="Convergence Criteria for alpha = 0.1, given")
ax1.plot(error, label="Error for alpha = 0.1, given")

ax2.plot(criteria1, label="Convergence Criteria for alpha = 1, given")
ax2.plot(error1, label="Error for alpha = 1, given")

ax3.plot(criteria2, label="Convergence Criteria for alpha = 1.01, given")
ax3.plot(error2, label="Error for alpha = 1.01, given")

ax1.grid(True)
ax1.legend()
ax2.grid(True)
ax2.legend()
ax3.grid(True)
ax3.legend()

plt.show()
