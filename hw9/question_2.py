import matplotlib.pyplot as plt
import numpy as np


def gradient_descent(x,alpha):
	criteria = []
	error = []
	errorx = []

	max_iterations = 30
	for i in range(30):
		criteria.append(-2*x*alpha)
		x = x - alpha*2*x
		error.append(abs(x-0))
		errorx.append(x**2)

	return (criteria,error,errorx)



alpha = 0.1
x = -2


criteria,error,errorx = gradient_descent(x,alpha)
criteria1, error1,errorx1 = gradient_descent(x, 1)
criteria2, error2, errorx2 = gradient_descent(x, 1.01)

fig = plt.figure()

ax1 = fig.add_subplot(111)

# ax1.plot(criteria,label = "Convergence Criteria for alpha = 0.1, given")
# ax1.plot(criteria1,label = "Convergence Criteria for alpha = 1, given")
# ax1.plot(criteria2,label = "Convergence Criteria for alpha = 1.01, given")
ax1.plot(errorx,label = "Error for alpha = 0.1, given")
ax1.plot(errorx1,label = "Error for alpha = 1, given")
ax1.plot(errorx2,label = "Error for alpha = 1.01, given")
ax1.grid(True)
ax1.legend()
print(errorx2)
plt.show()



