import numpy as np
import matplotlib.pyplot as plt
# from math import pi
import math

def part_a(s,k):
	samples = []
	for _ in range(s):
		samples.append(np.random.normal(0,1,k))
	MLE = []
	for sample in samples:
		MLE.append(np.mean(sample))
		# m = np.mean(sample)
		# var = np.var(sample)
		# mle = (1/math.sqrt(2*math.pi*var))*(math.exp((-1*(m**2))/(2*var)))
		# MLE.append(mle)
	# print(MLE)	
	var = np.var(MLE)
	return var

samples = []
for _ in range(1000):
	samples.append(np.random.normal(0, 1, 1000))
MLE = []
for sample in samples:
	MLE.append(np.mean(sample))

plt.plot(MLE)
plt.show()

s = 1000
varying_k = []
for k in range(1,1000):
	varying_k.append(part_a(s,k))


plt.plot(varying_k)
plt.grid(True)
plt.title("varying k")
plt.show()


k = 1000
varying_s = []
for s in range(1,1000):
	varying_s.append(part_a(s,k))


plt.plot(varying_s)
plt.grid(True)
plt.title("varying s")
plt.show()

