# -*- coding: utf-8 -*-
"""q1.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/140LIDB7KZWPMZ4a5DO-GGLFpTp45Sarj
"""

import torch
import torch.nn as nn
import torchvision
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.nn.init as weight_init
import matplotlib.pyplot as plt
import pdb
from torch.optim.lr_scheduler import StepLR

preprocess = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])

#Loading the train set file
train_dataset = datasets.MNIST(root='./data', 
                            train=True, 
                            transform=preprocess,  
                            download=True)
#Loading the test set file
test_dataset = datasets.MNIST(root='./data', 
                           train=False, 
                           transform=preprocess)

print(len(train_dataset))
print(train_dataset[0][0].size())

# Dataloader
batch_size = 1000

#loading the train dataset
train_loader = torch.utils.data.DataLoader(dataset=train_dataset, 
                                           batch_size=batch_size, 
                                           shuffle=True)

#loading the test dataset
test_loader = torch.utils.data.DataLoader(dataset=test_dataset, 
                                          batch_size=batch_size, 
                                          shuffle=False)

input_size = 784
hidden_size = 256
num_classes = 10
num_epochs = 10
# learning_rate = 0.01
momentum_rate = 0.9

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes,init_weight):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)
        self.softmax = nn.Softmax(dim=1)
        

        #Weight Initialization
        if init_weight=='x':
          for m in self.modules():
            if isinstance(m,nn.Linear):
              weight_init.xavier_normal_(m.weight)
        if init_weight=='u':
          for m in self.modules():
            if isinstance(m,nn.Linear):
              weight_init.uniform(m.weight,a=0,b=1)
        if init_weight == 'g':
          for m in self.modules():
            if isinstance(m,nn.Linear):
              weight_init.normal_(m.weight,mean=0.,std=1.)
    
    def forward(self, x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        out = self.softmax(out)
        return out

losses = []
for l in [10,1,0.1,0.01,0.0001,0.00001]:
    net = Net(input_size, hidden_size, num_classes,'x')
    #Loss function
    criterion = nn.CrossEntropyLoss()
    learning_rate = l    

    # SGD for Optimizer
    optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=momentum_rate)
    scheduler = StepLR(optimizer, step_size=1, gamma=0.1)
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    print('Using CUDA ', use_cuda)

    #Transfer to GPU device if available
    net = net.to(device)
    net

    # Training
    epochLoss = []
    for epoch in range(num_epochs):
        scheduler.step()
        total_loss = 0
        cntr = 1
        # For each batch of images in train set
        for i, (images, labels) in enumerate(train_loader):
            
            images = images.view(-1, 28*28)
            labels = labels
            
            images, labels = images.to(device), labels.to(device)
            
            # Initialize gradients to 0
            optimizer.zero_grad()
            
            # Forward pass (this calls the "forward" function within Net)
            outputs = net(images)
            
            # Find the loss
            loss = criterion(outputs, labels)
            
            # Backward pass (Find the gradients of all weights using the loss)
            loss.backward()
            
            # Update the weights using the optimizer
            # For e.g.: w = w - (delta_w)*lr
            optimizer.step()
            
            total_loss += loss.item()
            cntr += 1
        
        print('Epoch [%d/%d] Loss:%.4f'%(epoch+1, num_epochs, total_loss/cntr) )
        epochLoss.append(total_loss/cntr)
    losses.append(epochLoss)

#Other functions
lrs = [10,1,0.1,0.01,0.0001,0.00001]
# def loss_plot(losses):
#     max_epochs = len(losses)
#     times = list(range(1, max_epochs+1))
#     plt.figure(figsize=(30, 7))
#     plt.xlabel("epochs")
#     plt.ylabel("cross-entropy loss")  
#     return plt.plot(times, losses)

# _ = loss_plot(epochLoss)

fig = plt.figure()
plt.figure(figsize=(20,10))
for i in range(len(lrs)):
    plt.subplot(2,3,i+1)
    plt.title("Learning rate = "+str(lrs[i]))
    max_epochs = len(losses[i])
    times = list(range(1, max_epochs+1))
    plt.xlabel("epochs")
    plt.ylabel("cross-entropy loss")
    plt.plot(times,losses[i])

plt.show()

losses = []
for ini in ['x','u','g']:
    net = Net(input_size, hidden_size, num_classes,ini)
    #Loss function
    criterion = nn.CrossEntropyLoss()
    learning_rate = 0.1    

    # SGD for Optimizer
    optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=momentum_rate)
    scheduler = StepLR(optimizer, step_size=1, gamma=0.1)
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    print('Using CUDA ', use_cuda)

    #Transfer to GPU device if available
    net = net.to(device)
    net

    # Training
    epochLoss = []
    for epoch in range(num_epochs):
        scheduler.step()
        total_loss = 0
        cntr = 1
        # For each batch of images in train set
        for i, (images, labels) in enumerate(train_loader):
            
            images = images.view(-1, 28*28)
            labels = labels
            
            images, labels = images.to(device), labels.to(device)
            
            # Initialize gradients to 0
            optimizer.zero_grad()
            
            # Forward pass (this calls the "forward" function within Net)
            outputs = net(images)
            
            # Find the loss
            loss = criterion(outputs, labels)
            
            # Backward pass (Find the gradients of all weights using the loss)
            loss.backward()
            
            # Update the weights using the optimizer
            # For e.g.: w = w - (delta_w)*lr
            optimizer.step()
            
            total_loss += loss.item()
            cntr += 1
        
        print('Epoch [%d/%d] Loss:%.4f'%(epoch+1, num_epochs, total_loss/cntr) )
        epochLoss.append(total_loss/cntr)
    losses.append(epochLoss)

fig = plt.figure()
plt.figure(figsize=(20,10))
inits = ['xaviers','uniform','gaussian']
for i in range(3):
    plt.subplot(1,3,i+1)

    plt.title("Initialization = "+inits[i])
    max_epochs = len(losses[i])
    times = list(range(1, max_epochs+1))
    plt.xlabel("epochs")
    plt.ylabel("cross-entropy loss")
    plt.plot(times,losses[i])

plt.show()