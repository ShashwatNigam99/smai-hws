import numpy as np
data = np.array([[1,1,1,-1],\
                [-1,-1,1,-1],\
                [2,2,1,1],\
                [-2,-2,1,-1],\
                [-1,1,1,1],\
                [1,-1,1,1]])
lr = 0.5             
w = np.array([1,0,-1])
for i in range(10):
    print("Iteration",i)
    for p in data:
        pred = np.sign(w.T @ p[:3])
        actual = np.sign(p[3])
        if pred!= actual:
            print("misclassified ",p[:3])
            w = w + lr*actual*p[:3]
    print("New w is",w)
        



