import numpy as np
import matplotlib.pyplot as plt

data = []
with open("airfoil_self_noise.dat","r") as f:
    lines = f.readlines()
    for line in lines:
        row = line.split("\t")
        row = np.array(row).astype('float64')
        data.append(row)

mean = np.mean(data, axis=0)
norm_data = []

for row in data:
    t = []
    for i in range(6):
        t.append(row[i]/mean[i])
    norm_data.append(t)

def MSE_Gradient(w,data):
    neg_grad = np.zeros(6)
    N = len(data)
    for i in range(0, N):
        row = data[i][:5]
        row.append(1)
        
        x = np.array(row)
        y = data[5]
        neg_grad += ((y - np.dot(w,x)) * x)

    return -neg_grad

def MSE_Loss(w,data):
    loss = 0
    N = len(data)
    for i in range(0, N):
        row = data[i][:5]
        row.append(1)
        x = np.array(row)
        y = data[i][5]
        loss += (y - np.dot(w,x))**2

    return loss / N

def grad_descent(data):
    learn_rate = 0.0000001
    itr = 0
    w = np.zeros(6)
    
    loss = 10
    grad = MSE_Gradient(w, data)

    while abs(MSE_Loss(w, data) - loss ) > 10e-6 :
        loss = MSE_Loss(w, data)
        print("Iteration -> ", itr, ", Loss = ",loss)
        old_w = w
        w = w - learn_rate * (grad)
        grad = MSE_Gradient(w,data)
        size = np.linalg.norm(grad)
        itr += 1
        
    w = old_w
    print("Simple Gradient Descent took ",itr," iterations")
    return w,itr 
    
w1, itr1 = grad_descent(norm_data)
print(w1)
    

def get_optimum_learn_rate(grad, data):
    N = len(data)
    hessian = np.zeros([6,6])
    for i in range(0, N):
        row = data[i][:5]
        row.append(1)
        x = np.array(row)
        hessian += np.outer(x,x)

    learn_rate_opt = (np.linalg.norm(grad)**2)/(np.dot(grad,np.dot(hessian,grad)))
    return learn_rate_opt/N

def opt_grad_descent(data):
    itr = 0
    w = np.zeros(6)
    
    loss = 10
    grad = MSE_Gradient(w, data)

    while abs(MSE_Loss(w, data) - loss ) > 10e-6 :
        loss = MSE_Loss(w, data)
        print("Iteration -> ", itr, ", Loss = ",loss)
        old_w = w
        learn_rate = get_optimum_learn_rate(grad, data)
        w = w - learn_rate * (grad)
        grad = MSE_Gradient(w,data)
        size = np.linalg.norm(grad)
        itr += 1
        
    w = old_w
    print("Optimised Gradient Descent took ",itr," iterations")
    return w,itr 

w2, itr2 = opt_grad_descent(norm_data)
print(w2)

print("Output for Simple Gradient Descent: ", w1)
print("Output for Optimised Gradient Descent: ", w2)

