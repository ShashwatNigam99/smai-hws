import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
datax  = np.random.uniform(low=-500.0,high=500.0,size=(1000,1))
p = np.random.uniform(low=-300,high=300,size=(1000,1))
datay = 3*datax+ p + 5
meanx = np.mean(datax)
meany = np.mean(datay)
sigma = np.cov(datax.transpose(),datay.transpose())
eigv,ev = LA.eig(sigma)
fig = plt.figure()
plt.scatter(datax,datay,s = 1)
m = [meanx],[meany]
plt.quiver(*m,ev[:,0],ev[:,1],color=['r','g'],scale=3)
plt.show()