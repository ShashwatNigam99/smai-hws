#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier


# In[2]:


def sigmoid(z):
    return 1/(1 + np.exp(-z))


# In[3]:


def initialize_parameters(n_x, n_h, n_y,bias):
    W1 = np.random.randn(n_h, n_x)
    b1 = np.zeros((n_h, 1))
    W2 = np.random.randn(n_y, n_h)
    b2 = np.zeros((n_y, 1))

    parameters = {
        "W1": W1,
        "b1" : b1,
        "W2": W2,
        "b2" : b2
    }
    
    if bias==False:
        parameters = {
        "W1": W1,
        "W2": W2
        }
        
    return parameters


# In[4]:


def forward_prop(X, parameters,bias):
    W1 = parameters["W1"]
    W2 = parameters["W2"]
  
    if bias==False:
        Z1 = np.dot(W1, X)
        A1 = np.tanh(Z1)
        Z2 = np.dot(W2, A1)
        A2 = sigmoid(Z2)
    else:
        b1 = parameters["b1"]
        b2 = parameters["b2"]

        Z1 = np.dot(W1, X) + b1
        A1 = np.tanh(Z1)
        Z2 = np.dot(W2, A1) + b2
        A2 = sigmoid(Z2)
        
    cache = {
        "A1": A1,
        "A2": A2
    }
    return A2, cache


# In[5]:


def calculate_cost(A2, yy):
    cost = np.sum((A2-yy)**2)/m
    return cost


# In[6]:


def backward_prop(X, Y, cache, parameters,bias):
    A1 = cache["A1"]
    A2 = cache["A2"]

    W2 = parameters["W2"]

    dZ2 = (A2 - Y)*A2*(1-A2)
    dW2 = np.dot(dZ2, A1.T)/m
    db2 = np.sum(dZ2, axis=1, keepdims=True)/m

    dZ1 = np.multiply(np.dot(W2.T, dZ2), 1-np.power(A1, 2))
    dW1 = np.dot(dZ1, X.T)/m
    
    db1 = np.sum(dZ1, axis=1, keepdims=True)/m
    
    if bias==False:
        grads = {
        "dW1": dW1,
        "dW2": dW2,
        }
    else:
        grads = {
        "dW1": dW1,
        "db1": db1,
        "dW2": dW2,
        "db2": db2
        }

    return grads


# In[7]:


def update_parameters(parameters, grads, learning_rate,bias):
    W1 = parameters["W1"] - learning_rate*grads["dW1"]
    W2 = parameters["W2"] - learning_rate*grads["dW2"]
    if bias==True:
        b1 = parameters["b1"] - learning_rate*grads["db1"]
        b2 = parameters["b2"] - learning_rate*grads["db2"]
        new_parameters = {
        "W1": W1,
        "W2": W2,
        "b1" : b1,
        "b2" : b2
        }
    else:
        new_parameters = {
        "W1": W1,
        "W2": W2,
        }
        
    return new_parameters


# In[8]:


def model(X, Y, n_x, n_h, n_y, num_of_iters, learning_rate,bias):
    parameters = initialize_parameters(n_x, n_h, n_y,bias)
    
    costs = []
    
    for i in range(0, num_of_iters+1):
        a2, cache = forward_prop(X, parameters,bias)

        cost = calculate_cost(a2, Y)
        costs.append(cost)

        grads = backward_prop(X, Y, cache, parameters,bias)

        parameters = update_parameters(parameters, grads, learning_rate,bias)

        if(i%500 == 0):
            print('Cost after iteration# {:d}: {:f}'.format(i, cost))

    return parameters,costs


# In[9]:


def predict(X, parameters,bias):
    a2, cache = forward_prop(X, parameters,bias)
    return a2


# In[10]:


np.random.seed(2)

data=np.random.multivariate_normal([0,0],[[1,0],[0,1]],200)

A_data=np.random.multivariate_normal([0,0],[[1,0],[0,1]],100)
A_label=np.ones(100)
B_data=np.random.multivariate_normal([0,0],[[1,0],[0,1]],100)
B_label=np.zeros(100)
train_data=np.concatenate((A_data[0:80,:],B_data[0:80,:]),axis=0)
train_label=np.concatenate((A_label[0:80],B_label[0:80]))
test_data=np.concatenate((A_data[80:100,:],B_data[80:100,:]),axis=0)
test_label=np.concatenate((A_label[80:100],B_label[80:100]))
print(train_data.shape)
print(test_data.shape)
print(train_label.shape)
print(test_label.shape)


X = train_data.T
Y = train_label
X_test = test_data.T
Y_test = test_label

# No. of training examples
m = X.shape[1]

# Set the hyperparameters
n_x = 2     #No. of neurons in first layer
n_h = 2     #No. of neurons in hidden layer
n_y = 1     #No. of neurons in output layer
num_of_iters = 1000
learning_rate = 0.1

trained_parameters_withbias,costs_withbias = model(X, Y, n_x, n_h, n_y, num_of_iters, learning_rate,True)
trained_parameters_withoutbias,costs_withoutbias = model(X, Y, n_x, n_h, n_y, num_of_iters, learning_rate,False)


# In[11]:


y_predict_withbias = predict(X_test, trained_parameters_withbias,True)
y_predict_withoutbias = predict(X_test, trained_parameters_withoutbias,False)
testing_cost_withbias = calculate_cost(y_predict_withbias,Y_test)
testing_cost_withoutbias = calculate_cost(y_predict_withoutbias,Y_test)
print("Testing: mse error with bias:",testing_cost_withbias)
print("Testing: mse error without bias:",testing_cost_withoutbias)


# In[12]:


def error_calc(y_predict, Y,s):
    mask = y_predict>0.5
    y_predict[mask] = 1
    y_predict[np.logical_not(mask)] = 0
    score = 0
    for i in range(y_predict[0].shape[0]):
        if(Y_test[i]==y_predict[0][i]):
            score = score+1
    l = y_predict[0].shape[0]        
    print(s,score/l*100,"%")        


# In[13]:


error_calc(y_predict_withbias,Y_test,"Accuracy with bias")
error_calc(y_predict_withoutbias,Y_test,"Accuracy without bias")


# In[14]:


clf=MLPClassifier(hidden_layer_sizes=(2,),max_iter=10000,learning_rate_init=0.1)
clf.fit(train_data,train_label)


# In[15]:


y = clf.score(X_test.T,Y_test)
print("Accuracy with inbuilt",y*100)


# In[17]:


plt.plot(costs_withoutbias,label='Without Bias')
plt.plot(costs_withbias,label='With Bias')
plt.legend()
plt.show()


# In[20]:


plt.plot(clf.loss_curve_,label='Inbuilt')
plt.legend()
plt.show()

