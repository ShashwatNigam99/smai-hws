#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier


# In[2]:


def read_data(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    
    num_points = len(lines)
    dim_points = 28 * 28
    data = np.empty((num_points, dim_points))
    labels = np.empty(num_points)
    
    for ind, line in enumerate(lines):
        num = line.split(',')
        labels[ind] = int(num[0])
        data[ind] = [ int(x) for x in num[1:] ]
        
    return (data, labels)

train_data, train_labels = read_data("sample_train.csv")
test_data, test_labels = read_data("sample_test.csv")
print(train_data.shape, test_data.shape)
print(train_labels.shape, test_labels.shape)


# In[3]:


x = train_data[train_labels == 1]
y = train_data[train_labels == 2]

zero_row = np.zeros(x.shape[0])
one_row = np.ones(y.shape[0])

train_data = np.concatenate((x, y), axis = 0) 
train_label = np.concatenate((zero_row, one_row), axis = 0)

test_x = test_data[test_labels == 1]
test_y = test_data[test_labels == 2]

zero_row = np.zeros(test_x.shape[0])
one_row = np.ones(test_y.shape[0])

test_data = np.concatenate((test_x, test_y), axis = 0) 
test_label = np.concatenate((zero_row, one_row), axis = 0)

print(train_data.shape)
print(train_label.shape)

print(test_data.shape)
print(test_label.shape)


# In[4]:


clf = MLPClassifier(hidden_layer_sizes=(1000, 1000), max_iter=10000, learning_rate_init=0.1)
clf.fit(train_data,train_label)


# In[5]:


correct = 0
total = test_data.shape[0]
y_predicted = clf.predict(test_data)

for i in range(len(test_data)):
    if test_label[i] == y_predicted[i]:
        correct += 1

accuracy = correct/ total * 100
print(accuracy)

