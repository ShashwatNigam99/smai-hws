#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import print_function
from numpy import linalg as LA
from math import log,pi
import numpy as np


# In[9]:


def read_data(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    
    num_points = len(lines)
    dim_points = 28 * 28
    data = np.empty((num_points, dim_points))
    labels = np.empty(num_points)
    
    for ind, line in enumerate(lines):
        num = line.split(',')
        labels[ind] = int(num[0])
        data[ind] = [ int(x) for x in num[1:] ]
        
    return (data, labels)

train_data, train_labels = read_data("sample_train.csv")
test_data, test_labels = read_data("sample_test.csv")


# In[10]:


y = np.zeros((6000,10))
y[np.arange(6000),np.array(train_labels[:].astype(int))] = 1
y = y.astype(float)


# In[11]:


train_data = (train_data-np.mean(train_data,axis=0))/np.var(train_data)
test_data= (test_data-np.mean(test_data,axis=0))/np.var(test_data)


# In[12]:


def grad(w):
    grad = []
    L = np.exp(w @ train_data.T)
    sum_L = np.sum(L, axis = 0)
    for i in range(10):
        grad.append(((L[i,:]/sum_L)-y.T[i]).T @ train_data)
    return np.array(grad)


# In[13]:


def logistic_gd(lr,iters):
    w=np.random.uniform(size=(10,784))
    for i in range(iters):
        w = w - lr*grad(w)
    return w


# In[14]:


def check(w):
    L = np.exp(w @ test_data.T)
    sum_L = np.sum(L, axis = 0)
    return np.argmax(L/sum_L, axis=0)


# In[15]:


accuracy=0
out=check(logistic_gd(1,100))
for i in range(1000):
    if(out[i] == test_labels[i]):
        accuracy= accuracy+ 1
print(accuracy/10)

