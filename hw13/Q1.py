#!/usr/bin/env python
# coding: utf-8

# In[2]:


from sklearn.linear_model import Perceptron,LogisticRegression
import numpy as np
import matplotlib.pyplot as plt


# In[3]:


x = np.array([[1,2],[2,3],[1,9],[2,6]])
y = np.array([0,0,1,1])


# In[4]:


ppn = Perceptron(max_iter=30, eta0=0.1)
ppn.fit(x, y)
per = ppn.coef_[0]
bias = ppn.intercept_[0]
xx = np.arange(10)
yy = -1*(per[0]*xx+bias)/per[1]


# In[5]:


plt.scatter(x[:2,0],x[:2,1])
plt.scatter(x[2:,0],x[2:,1])
plt.plot(xx,yy)
plt.title('Perceptron Algorithm')
plt.show()


# In[6]:


def logg(n):
    lr = LogisticRegression(C=80)
    lr.fit(n*x,y)
    w_log = lr.coef_[0]
    bias = lr.intercept_[0]
    xl = np.arange(10)
    yl = -1*(w_log[0]*xl+bias)/w_log[1]
    return (xl,yl)


# In[9]:


plt.scatter(x[:2,0],x[:2,1])
plt.scatter(x[2:,0],x[2:,1])
plt.plot(logg(1)[0],logg(1)[1])
plt.title('Logistic Regression, Gamma=1')
plt.show()
plt.scatter(x[:2,0],x[:2,1])
plt.scatter(x[2:,0],x[2:,1])
plt.plot(logg(0.5)[0],logg(0.5)[1])
plt.title('Logistic Regression, Gamma=0.5')
plt.show()
plt.scatter(x[:2,0],x[:2,1])
plt.scatter(x[2:,0],x[2:,1])
plt.plot(logg(2)[0],logg(2)[1])
plt.title('Logistic Regression, Gamma=2')
plt.show()

