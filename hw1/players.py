import pandas as pd
import numpy as np
import math
df = pd.read_excel ('./icc.xlsx') 

sangakarra = [41,1.78,3,41.98,200,404]
sang_diff = list()
warner = [32,1.7,1,46.12,200,116]
warner_diff = list()
gayle = [39,1.88,1,37.93,35.48,298]
gayle_diff = list()

for i in df.values:
    sang_diff.append(math.sqrt(sum(np.power( i[1:]-sangakarra,2 ))))
    warner_diff.append(math.sqrt(sum(np.power( i[1:]-warner,2 ))))
    gayle_diff.append(math.sqrt(sum(np.power( i[1:]-gayle,2 ))))

print(sang_diff)
print()
print(warner_diff)
print()
print(gayle_diff)


print("Player most similar to Sangakkara:",df.values[sang_diff.index(min(sang_diff))][0])
print("Player most similar to Warner:",df.values[warner_diff.index(min(warner_diff))][0])
print("Player most similar to Gayle:",df.values[gayle_diff.index(min(gayle_diff))][0])