import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA

# data = np.array([[0,100,100,1],[0,1,100,100]]);
data = np.array([[0,0,2,2],[0,1,0,1]]);


print(data)
cov = np.cov(data)
print(cov)
mean = np.mean(data,axis=1)
eigvals, ev = LA.eig(cov)
print(eigvals)
print(ev)
m = [mean[0]],[mean[1]]
plt.scatter(data[0,:],data[1,:])
plt.quiver(*m,ev[:,0],ev[:,1],color=['r','g'],scale=3)
plt.show()   

