import numpy as np
from sklearn.svm import SVC
import matplotlib.pyplot as plt
from sklearn import svm

X = np.array([
    [ 1, 1],
    [-1,-1],
    [-1, 1],
    [ 1,-1],
])

Y = np.array([
    1,
    1,
    -1,
    -1,
])

def make_meshgrid(x, y, h=.01):
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    return xx, yy

def plot_contours(ax, clf, xx, yy, **params):
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out


kernels = ['rbf', 'poly', 'sigmoid']
C = [1e-10, 1, 1e5]

i = -1
j = 0
fig, ax = plt.subplots(3,3)

for kernel in kernels:
    i = i + 1
    j = 0
    for c in C:

        model = SVC(kernel=kernel, C=c)
        clf = model.fit(X, Y)

        title = ("SVM Decision Boundary with {} kernel and C = {}".format(kernel, c))
        xx, yy = make_meshgrid(X[:, 0], X[:, 1])
        plot_contours(ax[i,j], clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
        ax[i,j].scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.coolwarm, s=20, edgecolors='k')
        ax[i,j].set_xticks(())
        ax[i,j].set_yticks(())
        ax[i,j].set_title(title)
        j = j+1   
        
plt.show()