#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import csv
import matplotlib.pyplot as plt


# In[3]:


def read_data(filename):
    X = []
    Y = []
    with open(filename, 'r') as file:
        dataset = csv.reader(file)
        for row in dataset:
            X.append(row[1:])
            Y.append([row[0]])
        X = np.array(X).astype(float)
        Y = np.array(Y).astype(int)
    return (X, Y)


# In[4]:


(X, Y) = read_data('wine.data')


# In[9]:


C = np.cov(X.T)
(V, D) = np.linalg.eigh(C)
V = V[::-1]
W = V / np.sum(V)*100
D = D.T[::-1].T
print(W)
plt.plot(V,"bo")
plt.show()


# In[12]:


P = D[:, :2]
proj = X @ P

x = proj[:, 0]
y = proj[:, 1]

c1 = np.where(Y == 1)[0]
c2 = np.where(Y == 2)[0]
c3 = np.where(Y == 3)[0]

print('Red eigenvector: 1st principal component')
print('Green eigenvector: 2nd principal component')
plt.figure(figsize=(10, 10))
plt.quiver(np.mean(x), np.mean(y), P[:, 0], P[:, 1], color=['r', 'g','b'], scale=10)
plt.scatter(x[c1], y[c1], marker='1', label='Class 1')
plt.scatter(x[c2], y[c2], marker='2', label='Class 2')
plt.scatter(x[c3], y[c3], marker='3', label='Class 3')
plt.legend()
plt.grid()
plt.show()

