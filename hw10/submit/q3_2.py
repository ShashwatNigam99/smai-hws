#%%
#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pickle
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


# In[6]:


data_dict = unpickle('./data/data_batch_1')
data = data_dict[b'data']
labels = data_dict[b'labels']
for i in range(2,6):
    data_dict = unpickle('./data/data_batch_'+str(i))
    data = np.concatenate((data, data_dict[b'data']))
    labels = np.concatenate((labels, data_dict[b'labels']))


# In[ ]:


data = data[:,]


# In[4]:


classes = []
means = []
for i in range(10):
    classes.append(data[labels==i])
for i in range(10):
    means.append(np.mean(classes[i],0))


# In[5]:

emat = np.zeros([10,10])
for i in range(0,10):
    for j in range(i+1,10):
        dist= np.linalg.norm(means[i]-means[j])
        emat[i][j] = dist
        emat[j][i] = dist
        # print("Distance between mean of", i , "and", j, "is", dist)
