#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pickle
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


# In[3]:


data_dict = unpickle('./data/data_batch_1')
data = data_dict[b'data']
labels = data_dict[b'labels']
for i in range(2,6):
    data_dict = unpickle('./data/data_batch_'+str(i))
    data = np.concatenate((data, data_dict[b'data']))
    labels = np.concatenate((labels, data_dict[b'labels']))


# In[4]:


data=(data[:,0:1024]+data[:,1024:2048]+data[:,2048:3072])/3


# In[5]:


classes = []
means = []
for i in range(10):
    classes.append(data[labels==i])
for i in range(10):
    means.append(np.mean(classes[i],0))


# In[6]:


pc20_eigvecs_classes = []
pc20_eigvals_classes = []
for i in range(10):
    class_data = classes[i]
    class_mean = means[i]
    class_cov  = np.cov(class_data.T)
    evals,evecs = np.linalg.eigh(class_cov)
    evals = evals[::-1]
    evecs = evecs.T[::-1].T
    pc20_eigvecs_classes.append(evecs[:,:20])
    pc20_eigvals_classes.append(evals[:20])


# In[7]:


projected_class_data = []
for i in range(10):
    reduced = np.matmul(classes[i],pc20_eigvecs_classes[i])
    projected_class_data.append(reduced)


# In[8]:


errors = []
for i in range(10):
    # taking norms along rows(images)
    norms_og = np.linalg.norm(classes[i],axis=1)
    # taking norms of projected
    norms_proj = np.linalg.norm(projected_class_data[i],axis=1)
    error = (1/5000)*( np.sum(np.sqrt( np.square(norms_og)-np.square(norms_proj) )) )
    errors.append(error)


# In[9]:


def calc_error(ca,ma,pc20b):
    reduced = ma + np.matmul(ca-ma,pc20b) @ pc20b.T
    error = (1/5000)*( np.sum(np.linalg.norm(ca-reduced,axis=1)) )
    return error
        


# In[10]:


E_mat = np.zeros([10,10])
for i in range(10):
    for j in range(10):
        if i==j:
            E_mat[i][j] = 9999999
        if i!=j:
            E_mat[i][j] = calc_error(classes[i],means[i],pc20_eigvecs_classes[j])         


# In[11]:


sim_mat = (E_mat + E_mat.T)/2


# In[12]:


for i in range(10):
    print("For class",i,",similar classes:",np.argsort(sim_mat[i])[:3])

